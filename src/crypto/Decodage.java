package crypto;
// Nom : BARKIRE NIANDOU Mohamed
public class Decodage {
		static final String m1 = "BB3A65F6F0034FA957F6A767699CE7FABA855AFB4F2B520AEAD612944A801E";
		static final String m2 = "BA7F24F2A35357A05CB8A16762C5A6AAAC924AE6447F0608A3D11388569A1E";
		static final String m3 = "A67261BBB30651BA5CF6BA297ED0E7B4E9894AA95E300247F0C0028F409A1E";
		static final String m4 = "A57261F5F0004BA74CF4AA2979D9A6B7AC854DA95E305203EC8515954C9D0F";
		static final String m5 = "BB3A70F3B91D48E84DF0AB702ECFEEB5BC8C5DA94C301E0BECD241954C831E";
		static final String m6 = "A6726DE8F01A50E849EDBC6C7C9CF2B2A88E19FD423E0647ECCB04DD4C9D1E";
		static final String m7 = "BC7570BBBF1D46E85AF9AA6C7A9CEFA9E9825CFD5E3A0047F7CD009305A71E";
		static final String cle ="F21A45D28300000005F0000000000000000000000000000000000000000000";
	   		
	    public static String stringAsHexFromBytes(byte buf[]) {
	        StringBuilder strbuf = new StringBuilder(buf.length * 2);
	        int i;
	        for (i = 0; i < buf.length; i++) {
	            if (((int) buf[i] & 0xff) < 0x10) {
	                strbuf.append("0");
	            }
	            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
	        }
	        return strbuf.toString();
	    }

	    
	    public static byte[] bytesFromStringAsHex(String hex) {
	        byte[] buf;

	        Integer hexlen = hex.length() / 2;
	        buf = new byte[hexlen];
	        int i;
	        for (i = 0; i < hexlen; i++) {
	            String str = hex.substring(i * 2, i * 2 + 2);

	            buf[i] = (byte) ((Character.digit(str.charAt(0), 16) << 4) + Character.digit(str.charAt(1), 16));
	        }
	        return buf;
	    }

	    public static String printByteArray( byte [] message){
	    	String str="";
	    	for(int i=0; i<message.length; i++){
	    		if(((char) (message[i])<32)||((char) (message[i])>128)){
	    			str+= ". ";
	    		}
	    		else{
	    		str+= (char) (message[i])+" ";  
	    		}
	    	}
	    	return str;	
	    }
	    public static void main(String[] args) {
	    	byte [] buffer1= new byte [1024];
	    	byte [] buffer2= new byte [1024];
	    	byte [] buffer3= new byte [1024];
	    	
	    	buffer1= Decodage.bytesFromStringAsHex(m1);
	    	buffer2= Decodage.bytesFromStringAsHex(m2);
	    	byte [] result= new byte[buffer1.length];
	    	int i=0;
	    	
	    	// Decodage message 1
		       buffer3= Decodage.bytesFromStringAsHex(cle);
		       for(int j=0;j < buffer1.length; j++){
		    		result[j]=  (byte) (buffer1[j]^buffer3[j]);
		    		i++;
		    	}
		       System.out.println(Decodage.printByteArray(result));
		       
	    	for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer1[j]^buffer2[j]);
	    		i++;
	    	}
	    	String resultat=Decodage.stringAsHexFromBytes(result);
	       System.out.println(resultat);

	    // Decodage message 2
	       buffer3= Decodage.bytesFromStringAsHex(cle);
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer2[j]^buffer3[j]);
	    		i++;
	    	}
	       System.out.println(Decodage.printByteArray(result));
	       
	       buffer2= Decodage.bytesFromStringAsHex(m3);
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer1[j]^buffer2[j]);
	    		i++;
	    	}
	    	resultat=Decodage.stringAsHexFromBytes(result);
	       System.out.println(resultat);
	       
	    // Decodage message 3
	      
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer3[j]^buffer2[j]);
	    		i++;
	    	}
	       System.out.println(Decodage.printByteArray(result));
	       
	       buffer2= Decodage.bytesFromStringAsHex(m4);
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer1[j]^buffer2[j]);
	    		i++;
	    	}
	    	resultat=Decodage.stringAsHexFromBytes(result);
	       System.out.println(resultat);
	       
	    // Decodage message 4
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer3[j]^buffer2[j]);
	    		i++;
	    	}
	       System.out.println(Decodage.printByteArray(result));
	       
	       buffer2= Decodage.bytesFromStringAsHex(m5);
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer1[j]^buffer2[j]);
	    		i++;
	    	}
	    	resultat=Decodage.stringAsHexFromBytes(result);
	       System.out.println(resultat);
	       
	    // Decodage message 5
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer3[j]^buffer2[j]);
	    		i++;
	    	}
	       System.out.println(Decodage.printByteArray(result));
	       
	       buffer2= Decodage.bytesFromStringAsHex(m6);
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer1[j]^buffer2[j]);
	    		i++;
	    	}
	    	resultat=Decodage.stringAsHexFromBytes(result);
	       System.out.println(resultat);
	       
	       // Decodage message 6
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer3[j]^buffer2[j]);
	    		i++;
	    	}
	       System.out.println(Decodage.printByteArray(result));
	       
	       buffer2= Decodage.bytesFromStringAsHex(m7);
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer1[j]^buffer2[j]);
	    		i++;
	    	}
	    	resultat=Decodage.stringAsHexFromBytes(result);
	       System.out.println(resultat);
	      // System.out.println(i);
	       
	       // Decodage message 7
	       for(int j=0;j < buffer1.length; j++){
	    		result[j]=  (byte) (buffer3[j]^buffer2[j]);
	    		i++;
	    	}
	       System.out.println(Decodage.printByteArray(result));
	    }
}
